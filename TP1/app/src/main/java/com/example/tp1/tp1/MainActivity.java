package com.example.tp1.tp1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String MESSAGE_PARAMETER = "message";
    private static final int REQUEST_CODE = 1;
    private Button addButton;
    private Button subButton;
    private TextView resultTextView;
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addButton = findViewById(R.id.btnadd);
        subButton = findViewById(R.id.btnsub);
        resultTextView = findViewById(R.id.txt);
        txtResult = findViewById(R.id.txtresult);

        /*addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //log.v("MainActivity", "Add button clicked");
                String text = resultTextView.getText().toString();
                int result = Integer.parseInt(resultTextView.getText().toString());
                int textNumber = Integer.parseInt(text);
                textNumber++;
                resultTextView.setText(Integer.toString(textNumber));
            }
        });*/

        /*subButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //log.v("MainActivity", "Sub button clicked");
                String text = resultTextView.getText().toString();
                int result = Integer.parseInt(resultTextView.getText().toString());
                int textNumber = Integer.parseInt(text);
                textNumber--;
                resultTextView.setText(Integer.toString(textNumber));
            }
        });*/
        addButton.setOnClickListener(this);
        subButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        String text = resultTextView.getText().toString();
        int textNumber = Integer.parseInt(text);
        if(v == addButton){
            textNumber++;
        }
        else {
            textNumber--;
        }
        resultTextView.setText(Integer.toString(textNumber));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }

    public void onClickShowPopup(View view) {
        Toast.makeText(this, "SALUUUUTTT", Toast.LENGTH_SHORT).show();
    }

    public void onClickShowSnackBar(View view) {
        Snackbar.make(view, "WESSHHHH", Snackbar.LENGTH_SHORT)
            .setAction("MisterFogo", view1 -> {
                Toast.makeText(this, "Heyyyyyy", Toast.LENGTH_LONG).show();
            })
                .show();
    }


    public void onClickActivities(View view){
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    public void onClickActivities2(View view){
        Intent intent = new Intent(this, ThirdActivity.class);
        intent.putExtra(MESSAGE_PARAMETER, "Hello from MainActivity");
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if(resultCode== REQUEST_CODE){
            if(resultCode== Activity.RESULT_OK){
            }
        }*/
        String result=data.getStringExtra("result");
        txtResult.setText(result);
    }
}