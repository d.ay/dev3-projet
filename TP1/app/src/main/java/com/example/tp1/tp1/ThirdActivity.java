package com.example.tp1.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        Bundle extras = getIntent().getExtras();
        String message = extras.getString("message");
        textView = findViewById(R.id.txt);
        textView.setText(message);
    }

    public void onClickOuiButton(View view) {
        Log.v("ThirdActivity", "Add button clicked");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "I'M READY");
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void onClickNonButton(View view) {
        Log.v("ThirdActivity", "Substract button clicked");
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "I'M NOT READY");
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();

    }
}