package com.example.tp1.tp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {


    private SeekBar seekBar;
    private ProgressBar progressBar;
    private TextView compteur;

    private boolean m_couting= true;

    private int compteurOnClick=0;
    private Timer timer;

    private MyHandler m_handler;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = findViewById(R.id.SB);
        progressBar= findViewById(R.id.PB);
        compteur = findViewById(R.id.tv);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        m_handler = new MyHandler();


    }

    public void ClickBtn(View view){
        compteurOnClick = Integer.parseInt(compteur.getText().toString());
        compteurOnClick++;
        compteur.setText(Integer.toString(compteurOnClick));
        progressBar.setProgress(compteurOnClick);
    }

    public void ClickBtn2(View view, IncrementAndDisplayCounterThreads myThread) {
        myThread = new IncrementAndDisplayCounterThreads();
        myThread.start();
    }

    public void ClickBtn3(View view) {
        if(((ToggleButton)view).isChecked()){
            timer = new Timer();
            timer.scheduleAtFixedRate(new MyTimerTask(), new Date(), 1000);
        }else{
            timer.cancel();
            timer = null;
        }
    }

    class IncrementAndDisplayCounterThreads extends Thread{
        public void run(){
            while(compteurOnClick < 100){
                compteurOnClick++;
                //showCounter();
                showCounterInMainThread();
                try{
                    Thread.sleep(1000);
                }
                catch (InterruptedException e){
                    throw new RuntimeException(e);
                }
            }
        }
    }

    private void showCounterInMainThread(){
        runOnUiThread(() -> showCounter());
    }
    private void showCounter(){
        progressBar.setProgress(compteurOnClick);
        compteur.setText(progressBar.getProgress()+"%");
    }

    class MyTimerTask extends TimerTask{
        @Override
        public void run(){
            compteurOnClick +=1;
            //showCounter();
            //showCounterInMainThread();
            //m_handler.sendEmptyMessage(0);
            sendMsg("in progress...");



        }
    }

    public void sendMsg(String msg){
        Message msgObj = m_handler.obtainMessage();
        Bundle b = new Bundle();
        b.putString("message", msg);
        b.putInt("counter",compteurOnClick);
        msgObj.setData(b);
        m_handler.sendMessage(msgObj);
    }

    private class MyHandler extends Handler{

        @Override
        public void handleMessage(@NonNull Message msg) {
            //showCounter();
            String aResponse = msg.getData().getString("message", "Empty");
            int counterToDisplay = msg.getData().getInt("counter", compteurOnClick);

            Log.v("TP", "Message Received;" + aResponse);
            compteur.setText(String.valueOf(counterToDisplay));

            if(counterToDisplay <= 100){
                progressBar.setProgress(counterToDisplay);
            }

        }
    }
}