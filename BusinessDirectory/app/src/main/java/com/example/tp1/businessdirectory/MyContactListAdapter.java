package com.example.tp1.businessdirectory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import java.util.List;

public class MyContactListAdapter extends BaseAdapter {

    private Context context;

    private List<BusinessContact> contacts;

    public MyContactListAdapter(Context context, List<BusinessContact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    // nombre item
    @Override
    public int getCount() {
        return this.contacts.size();
    }

    // retourner le contact
    @Override
    public Object getItem(int position) {
        return this.contacts.get(position);
    }

    // id de la bdd
    @Override
    public long getItemId(int position) {
        return this.contacts.get(position).getContactId();
    }

    // afficher les contacts
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*TwoLineListItem twoLineListItem;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            twoLineListItem = (TwoLineListItem) inflater.inflate(
                    android.R.layout.simple_list_item_2, null);
        } else {
            twoLineListItem = (TwoLineListItem) convertView;
        }

        TextView text1 = twoLineListItem.getText1();
        TextView text2 = twoLineListItem.getText2();

        BusinessContact contact = this.contacts.get(position);

        // TODO 11 set text1 with contact firstname and lastname

        // TODO 12 set text2 with contact address

        return twoLineListItem;*/


        View contactItem;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            contactItem = inflater.inflate(
                    R.layout.activity_main2, null);
        } else {
            contactItem = convertView;
        }

        TextView fullName= contactItem.findViewById(R.id.fullName);
        TextView address = contactItem.findViewById(R.id.address);

        BusinessContact contact = this.contacts.get(position);

        fullName.setText(contact.getFirstName() + " " + contact.getLastName());
        address.setText(contact.getAddress());

    return contactItem;
    }
}
