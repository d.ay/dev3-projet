package com.example.tp1.businessdirectory;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class CreateContactActivity extends AppCompatActivity {

    private MyBusContactDbHelper m_busContactDbHelper;

    private EditText m_firstName;
    private EditText m_lastname;
    private EditText m_address;
    private EditText m_phone;

    private BusinessContact m_contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);

        m_firstName = findViewById(R.id.firstName);
        // TODO 16 Get controlers for lastname, address and phone
        m_lastname = findViewById(R.id.lastName);
        m_address = findViewById(R.id.address);
        m_phone = findViewById(R.id.phone);

        m_busContactDbHelper = new MyBusContactDbHelper(this);

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            m_contact = (BusinessContact) bundle.getSerializable(MainActivity.EXTRA_CONTACT);

            // TODO 23 fill firstName lastName address and phone using Contact Getter
            m_firstName.setText(m_contact.getFirstName());
            m_lastname.setText(m_contact.getLastName());
            m_address.setText(m_contact.getAddress());
            m_phone.setText(m_contact.getPhoneNumber());

        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        // TODO 17 set Toolbar Title with "Create New Contact"
        // or "Update Contact" if we are in update mode
        if (m_contact != null) {
            toolbar.setTitle("Update Contact");
        } else {
            toolbar.setTitle("Create New Contact");
        }
        // TODO 22 set Toolbar Title with "Create New Contact" or "Update Contact"
        setSupportActionBar(toolbar);

        // Show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    // TODO 18 Manage a menu in the toolBar on overriding method onCreateOptionsMenu
    // If we are on mode update contact : hide menu item create
    // and display update and delete
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.my_create_contact_menu, menu);

        if(m_contact != null) {
            menu.findItem(R.id.addBusinessContact).setVisible(false);
            menu.findItem(R.id.updateBusinessContact).setVisible(true);
            menu.findItem(R.id.deleteBusinessContact).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }


    // TODO 19 Manage the menu toolBar icon click on overriding method onOptionsItemSelected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.addBusinessContact) {
            // TODO 21 Create a new contact in database
            // and go back to MainActivity
            String firstName = m_firstName.getText().toString();
            String lastName = m_lastname.getText().toString();
            String address = m_address.getText().toString();
            String phone = m_phone.getText().toString();
            BusinessContact contact = new BusinessContact(firstName, lastName, address, phone);
            m_busContactDbHelper.addContact(contact);
            finish(); // ferme l'activité
        }
        else if(id== R.id.updateBusinessContact){
            // TODO 25 Update contact in database
            // and go back to MainActivity
            m_contact.setFirstName(m_firstName.getText().toString());
            m_contact.setLastName(m_lastname.getText().toString());
            m_contact.setAddress(m_address.getText().toString());
            m_contact.setPhoneNumber(m_phone.getText().toString());
            m_busContactDbHelper.updateContact(m_contact);
            finish(); // ferme l'activité
        }
        else if(id== R.id.deleteBusinessContact){
            // TODO 26 Delete contact in database
            // and go back to MainActivity
            m_busContactDbHelper.deleteContact(m_contact);
            finish(); // ferme l'activité
        }
        return super.onOptionsItemSelected(item);
    }
    // TODO 24 Manage update and delete buttons



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}