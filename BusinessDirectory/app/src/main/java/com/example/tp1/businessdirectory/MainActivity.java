package com.example.tp1.businessdirectory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.TaskStackBuilder;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_CONTACT = "BUSINESS_CONTACT";
    private ListView m_listView;
    private List<BusinessContact> m_contactList = new ArrayList<>();
    private MyContactListAdapter m_listAdapter;
    private MyBusContactDbHelper m_busContactDbHelper;


    protected void onCreate(Bundle savedInstanceState) {
        /*super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_listView = findViewById(R.id.listContacts);
        m_contactList = new ArrayList<>();
        m_contactList.add(new BusinessContact(1, "Derya", "AY", "123 Main St", "555-555-5555"));
        m_contactList.add(new BusinessContact(2, "Selena", "GOMEZ", "123 Main St QLF", "555-555-5555"));
        m_listAdapter = new MyContactListAdapter(this, this.m_contactList);
        this.m_listView.setAdapter(m_listAdapter);*/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get ListView object from xml
        this.m_listView = findViewById(R.id.listContacts);

        m_busContactDbHelper = new MyBusContactDbHelper(this);
        // TODO 10 use the createDefaultContactsIfNeeded from dbHelper to display default Contacts in main Activity

        this.m_contactList = m_busContactDbHelper.getAllContacts();

        m_listAdapter = new MyContactListAdapter(this, this.m_contactList);
        this.m_listView.setAdapter(m_listAdapter);

        // TODO 21 manage List Item Click to openActivity in order to modify or delete contact
        m_listView.setOnItemClickListener((parent, view, position, id) -> {
            BusinessContact contact = m_contactList.get(position);
            Intent intent = new Intent(this, CreateContactActivity.class);
            intent.putExtra(EXTRA_CONTACT, contact);
            startActivity(intent);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // TODO 20 Refresh m_contactList with ALL contacts available in database
        if(m_contactList != null) {
            m_contactList.clear();
            List<BusinessContact> list = m_busContactDbHelper.getAllContacts();
            m_contactList.addAll(list);
            m_listAdapter.notifyDataSetChanged();
        }

    }

    // TODO 13 Manage a menu in the toolBar on overriding method onCreateOptionsMenu
    // Create an xml file : New -> Android Resource File -> Resource Type : menu
    // Use this file to inflate menu here
    //
    // In the XML menu file : use an SVG icon 'ADD' found in the android SVG library
    // -> menu New -> Vector Asset -> (clip art)

    // TODO 14 Manage the menu toolBar icon click on overriding method onOptionsItemSelected
    // test id is icon Id and open activity : CreateContactActivity


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.addBusinessContact) {
            startActivity(new Intent(this, CreateContactActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }









}